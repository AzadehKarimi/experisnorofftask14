﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace Task14_palindromic
{
    class Program
    {
        static void Main(string[] args)
        {
           // FindMaxNumber is a method for finding the largest palindrome made from the product of two 3-digit numbers.
            FindMaxNumber();
        }
      
        // CheckIfPalindrom is a method for for getting the reverse of the number and checking if the number is palindromic 
        static bool CheckIfPalindrom(int myNumber)
        {
            int reverse = 0, temp;
            while (myNumber != 0)
            {
                temp = myNumber % 10;
                reverse = reverse * 10 + temp;
                myNumber /= 10;
                if (myNumber == reverse)
                {
                    return true;
                }
            }
            return false;
        }
        // FindMaxNumber is a method for finding the largest palindrome made from the product of two 3-digit numbers.
        //We would have to find max of 3 digits so consider the loop from (999 to 100).
        public static void FindMaxNumber()
        {
            var numbers = new List<int>();
            for (int i = 999; i > 100; i--)
            {
                for (int j = 999; j > 100; j--)
                {
                    int MaxNumber = i * j;
                    if (CheckIfPalindrom(MaxNumber))// Apply CheckIfPalindrom to check if the number is Palindrom
                    {
                        numbers.Add(MaxNumber);// Get the max number and add it to our list.
                    }
                }
            }
            //Done the resulte would be printed.
            Console.WriteLine($"The maximum is {numbers.Max()}");
        }

    }
}
